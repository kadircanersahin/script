// ==UserScript==
// @name         REX
// @namespace    rex
// @version      0.1.1
// @description  Rex
// @author       kadircanersahin
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function () {
    'use strict';


    // Arama motoru, Simge & Kategori ekle

    const searchEngines = [
        // Find
        { name: 'Google', url: 'https://google.com/search?q=%s', icon: 'https://i.ibb.co/mJGtMTD/cursor1.png', category: 'Search' },
        { name: 'Yandex', url: 'https://yandex.com/search/?text=', icon: 'https://yastatic.net/s3/home-static/_/7c/7ccfee6f1e81b14c5bef535d1ad7b7e0.png', category: 'Search' },
        { name: 'DDG', url: 'https://duckduckgo.com/?q=%s', icon: 'https://duckduckgo.com/favicon.ico', category: 'Search' },
        { name: 'Bing', url: 'https://bing.com/search?q=%s', icon: 'https://bing.com/favicon.ico', category: 'Search' },
        { name: 'Startpage', url: 'https://startpage.com/sp/search?query=', icon: 'https://startpage.com/sp/cdn/favicons/favicon--default.ico', category: 'Search' },
        { name: 'You', url: 'https://you.com/search?q=%s', icon: 'https://you.com/favicon/favicon-96x96.png', category: 'Search' },
        { name: 'Qwant', url: 'https://www.qwant.com/?q=%s', icon: 'https://www.qwant.com/public/favicon-32.ca1a5c064bd406c0d3d7976349c2df57.png', category: 'Search' },
        { name: 'Ecosia', url: 'https://ecosia.org/search?q=%s', icon: 'https://cdn-static.ecosia.org/static/icons/favicon.ico', category: 'Search' },
        { name: 'WolframAlpha', url: 'https://www.wolframalpha.com/input?i=', icon: 'https://www.wolframalpha.com/apple-touch-icon.png', category: 'Search' },

        // Search
        { name: 'Wikipedia', url: 'https://en.wikipedia.org/wiki/Special:Search?search=', icon: 'http://wikipedia.org/favicon.ico', category: 'Find' },
        { name: 'Archive', url: 'https://archive.org/search?query=', icon: 'https://archive.org/offshoot_assets/favicon.ico', category: 'Find' },
        { name: 'Archive (Web)', url: 'https://archive.org/search?query=', icon: 'https://archive.org/offshoot_assets/favicon.ico', category: 'Find' },
        { name: 'IMDb', url: 'https://www.imdb.com/find/?q=%s', icon: 'https://m.media-amazon.com/images/G/01/imdb/images-ANDW73HA/favicon_iPad_retina_167x167._CB1582158068_.png', category: 'Find' },
        { name: 'Anna s Archive', url: 'https://annas-archive.org/search?q=%s', icon: 'https://tr.annas-archive.org/favicon-32x32.png?hash=989ac03e6b8daade6d2d', category: 'Find' },
        { name: 'Versus', url: 'https://versus.com/search?q=%s', icon: 'https://images.versus.io/favicon/apple-touch-icon-180x180.png', category: 'Find' },
        { name: 'LRCLIB', url: 'https://lrclib.net/search/', icon: 'https://lrclib.net/favicon.ico', category: 'Find' },
        { name: 'Libgen', url: 'https://libgen.is/search.php?req=%s', icon: 'https://libgen.is/favicon.ico', category: 'Find' },
        { name: 'EmojiDB', url: 'https://emojidb.org/%s', icon: 'https://cdn.glitch.com/4733404f-fac7-44c0-9994-ad38268ee641%2Fopen-file-folder_1f4c2.png?v=1580048783399', category: 'Find' },

        // Images
        { name: 'Google Images', url: 'https://www.google.com/search?q=%s&tbm=isch&iflsig=AOEireoAAAAAZIsFOdrspmil_fT_inTett_oz5qsxNNQ&ved=0ahUKEwiL-P-Ol8X_AhW9QvEDHW4WDUsQ4dUDCAY&uact=5&sclient=img', icon: 'https://www.google.com/favicon.ico', category: 'Images' },
        { name: 'Yandex Images', url: 'https://yandex.com/images/search?text=%s', icon: 'https://yastatic.net/s3/home-static/_/7c/7ccfee6f1e81b14c5bef535d1ad7b7e0.png', category: 'Images' },
        { name: 'DDG Images', url: 'https://duckduckgo.com/?q=%s&hps=1&iax=images&ia=images', icon: 'https://duckduckgo.com/favicon.ico', category: 'Images' },
        { name: 'Bing Images', url: 'https://www.bing.com/images/search?q=%s', icon: 'https://www.bing.com/sa/simg/favicon-2x.ico', category: 'Images' },
        { name: 'Qwant Images', url: 'https://www.qwant.com/?t=images&q=%s', icon: 'https://www.qwant.com/public/apple-touch-icon.3cb42a20879f2449925f49fe84c74112.png', category: 'Images' },
        { name: 'You Images', url: 'https://you.com/search?q=%s&fromSearchBar=true&tbm=isch', icon: 'https://you.com/favicon/favicon-32x32.png', category: 'Images' },
        { name: 'Ecosia', url: 'https://www.ecosia.org/images?q=%s', icon: 'https://cdn-static.ecosia.org/static/icons/apple-touch-icon.png', category: 'Images' },

        // Video
        { name: 'YouTube', url: 'https://www.youtube.com/results?search_query=%s', icon: 'https://www.youtube.com/s/desktop/cfd03dbf/img/favicon_96x96.png', category: 'Video' },
        { name: 'Odysee', url: 'https://odysee.com/$/search?q=%s', icon: 'https://odysee.com/public/favicon_128.png', category: 'Video' },
        { name: 'D.Tube', url: 'https://d.tube/#!/s/%s', icon: 'https://d.tube/DTube_files/images/dtubefavicon.png', category: 'Video' },
        { name: 'VK', url: 'https://vk.com/search?c%5Badult%5D=1&c%5Bper_page%5D=40&c%5Bq%5D=%s&c%5Bsection%5D=video&c%5Bsort%5D=2', icon: 'https://vk.com/favicon.ico', category: 'Video' },
        { name: 'Twitch', url: 'https://www.twitch.tv/search?term=%s', icon: 'https://www.twitch.tv/favicon.ico', category: 'Video' },
        { name: 'DailyMotion', url: 'https://www.dailymotion.com/search/%s/videos', icon: 'https://static1.dmcdn.net/neon/prod/favicons/apple-icon-precomposed.a966d521999d793bdfcbe436bb56dad4.png', category: 'Video' },
        { name: 'Coub', url: 'https://coub.com/search/channels?q=%s', icon: 'https://coub.com/apple-touch-icon-76.png', category: 'Video' },
        { name: 'TikTok', url: 'https://www.tiktok.com/search?q=%s', icon: 'https://www.tiktok.com/favicon.ico', category: 'Video' },
        { name: 'Rutube', url: 'https://rutube.ru/search/?query=', icon: 'https://static.rutube.ru/static/favicon.ico', category: 'Video' },

        // Social
        { name: 'X', url: 'https://twitter.com/search?q=%s', icon: 'https://abs.twimg.com/favicons/twitter.3.ico', category: 'Social' },
        { name: 'Reddit', url: '   ', icon: '   ', category: 'Social' },
        { name: 'Pinterest', url: '   ', icon: '   ', category: 'Social' },
        { name: 'Facebook', url: '   ', icon: '   ', category: 'Social' },
        { name: 'VSCO', url: '   ', icon: '   ', category: 'Social' },
        { name: 'Behance', url: '   ', icon: '   ', category: 'Social' },
        { name: 'Dribbble', url: '   ', icon: '   ', category: 'Social' },
        { name: 'Patreon', url: '   ', icon: '   ', category: 'Social' },


        // Eklenebilir...
    ];



    // Kayıp ikonlar için varsayılan ikon
    const defaultIconUrl = 'https://i.ibb.co/mJGtMTD/cursor1.png';

    for (const engine of searchEngines) {
      if (!engine.icon) {
        engine.icon = defaultIconUrl;
      }
    }

    // Kayıp isimler için varsayılan ad
    const defaultName = 'Empty 💀';

    for (let i = 0; i < searchEngines.length; i++) {
      if (!searchEngines[i].name) {
        searchEngines[i].name = defaultName;
      }
    }

     // Kayıp linkler için varsayılan URL
    const defaultUrl = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';

    for (const engine of searchEngines) {
      if (!engine.url) {
        engine.url = defaultUrl;
      }
    }


    // REX Conteiner

    const container = document.createElement('div');
    container.style.position = 'fixed';
    container.style.display = 'flex';
    container.style.top = '5px'; // Rex ile tepe arasındaki boşluk
    container.style.left = '50%'; // Rex'i ortala
    container.style.transform = 'translateX(-50%)';
    container.style.zIndex = '9999'; // Set a high z-index value to ensure it appears on top of other elements
    document.body.appendChild(container);
    // container.style.left = '50%'; İLE container.style.transform = 'translateX(-50%)'; AYNI ANDA KULLANMA MK.


    // Create the main REX category dropdown

    const mainDropdown = document.createElement('div');
    mainDropdown.style.display = 'inline-block';
    mainDropdown.style.marginLeft = '5px';
    container.appendChild(mainDropdown);


    // REX ikon adresleri

    const mainCategoryIconUrlNormal = 'https://i.ibb.co/mJGtMTD/cursor1.png'; // Rex normal icon URL (mouseout)
    const mainCategoryIconUrlHover = 'https://i.ibb.co/px9GYRw/cursor2.png'; // Rex hover icon URL (mouseover)



    // Create a button for the REX category

    const mainCategoryButton = document.createElement('button');
    mainCategoryButton.textContent = 'REX';
    mainCategoryButton.style.display = 'flex'; // Rex metnini dikey ortala
    mainCategoryButton.style.alignItems = 'center';
    mainCategoryButton.style.borderRadius = '20px'; // Oval kenarlar
    mainCategoryButton.style.backgroundColor = 'rgb(126,99,255)';
    mainCategoryButton.style.backdropFilter = 'blur(5px)'; // Glass efekt
    mainCategoryButton.style.border = 'none'; // Sınır olmaması için
    mainCategoryButton.style.padding = '10px 15px'; // İçerik etrafındaki boşluk
// ... ve diğer stiller

    mainDropdown.appendChild(mainCategoryButton);


    // REX butonu ikon ayarlamaları

    const mainCategoryIcon = document.createElement('img');
    mainCategoryIcon.src = mainCategoryIconUrlNormal;
    mainCategoryIcon.alt = 'REX Icon';
    mainCategoryIcon.style.marginRight = '10px'; // REX ikon ve metin arasındaki boşluk
    mainCategoryIcon.style.width = '20px'; // Rex ikon boyutu
    mainCategoryButton.insertBefore(mainCategoryIcon, mainCategoryButton.firstChild);


    // Ana kategori menüleri

    const mainMenu = document.createElement('div');
    mainMenu.style.position = 'fixed';
    mainMenu.style.display = 'flex'
    mainMenu.style.flexDirection = 'row';
    mainMenu.style.left = '50%';
    mainMenu.style.transform = 'translateX(-50%)';
    mainMenu.style.backgroundColor = '';
    mainMenu.style.padding = '5px'; // Kategori çerçeve genişliği
    mainMenu.style.border = '5px solid #';
    mainDropdown.appendChild(mainMenu);



    // Kategori ekle, sil veya düzenle

    const subcategories = ['Search', 'Find', 'Images', 'Video', 'Social'];

    subcategories.forEach(subcategory => {
        const subDropdown = document.createElement('div');
        subDropdown.style.display = 'inline-block';
        subDropdown.style.marginRight = '5px'; // Kategori düğmeleri arasındaki boşluk
        mainMenu.appendChild(subDropdown);


        // Ana kategori butonları

        const subCategoryButton = document.createElement('button');
        subCategoryButton.textContent = subcategory;
        subCategoryButton.style.marginRight = '5px'; // Kategoriler arasındaki boşluk

        // Vertically center align the text within the subcategory button

        subCategoryButton.style.display = 'flex'; // Menü yazılarını dikey ortala...
        subCategoryButton.style.alignItems = 'center'; // ...Menü yazılarını dikey ortala

        subCategoryButton.style.borderRadius = '20px'; // Oval kenarlar
        subCategoryButton.style.backgroundColor = '#7e63ff';
        subCategoryButton.style.backdropFilter = 'blur(15px)'; // Glass efekt
        subCategoryButton.style.border = 'none'; // Sınır olmaması için
        subCategoryButton.style.padding = '10px 15px';

        // ... ve diğer stiller
        subDropdown.appendChild(subCategoryButton);


        // Add an icon to the subcategory button based on the subcategory

        let subCategoryIconUrl = '';

        if (subcategory === 'Search') {
            subCategoryIconUrl = 'https://google.com/favicon.ico';
        }
        if (subcategory === 'Find') {
            subCategoryIconUrl = 'https://archive.org/offshoot_assets/favicon.ico';
        }
        if (subcategory === 'Images') {
            subCategoryIconUrl = 'https://i.ibb.co/mJGtMTD/cursor1.png';
        }
        if (subcategory === 'Video') {
            subCategoryIconUrl = 'https://www.youtube.com/s/desktop/cfd03dbf/img/favicon_96x96.png';
        }
        if (subcategory === 'Social') {
            subCategoryIconUrl = 'https://abs.twimg.com/favicons/twitter.3.ico';
        }


        // Add an icon to the subcategory button

        if (subCategoryIconUrl) {
            const subCategoryIcon = document.createElement('img');
            subCategoryIcon.src = subCategoryIconUrl;
            subCategoryIcon.alt = `${subcategory} Icon`;
            subCategoryIcon.style.marginRight = '5px';
            subCategoryIcon.style.width = '20px'; // Kategori ikon boyutu
            subCategoryButton.insertBefore(subCategoryIcon, subCategoryButton.firstChild);
        }


        // Arama motorları menüsü

        const subMenu = document.createElement('div');
        subMenu.style.display = 'none';
        subMenu.style.backgroundColor = '';
        subMenu.style.padding = '5px';
        subDropdown.appendChild(subMenu);


        // Add search engines to the subcategory menu based on the subcategory (Ek olarak, subCategoryButton üzerinde display: flex ayarının yapılması, içeriğinin (metnin) düğme içinde dikey olarak ortalanmasını sağlar.)

        // Arama motoru düğmeleri

        searchEngines.forEach(engine => {
        if (engine.category === subcategory) {
        const button = document.createElement('button');
        button.textContent = engine.name;

            // Vertically center align the text within the button

            button.style.display = 'flex'; // Dikey ortala...
            button.style.alignItems = 'center'; // ...Dikey ortala
            button.style.borderRadius = '20px'; // Oval kenarlar
            button.style.color = '#ffffff';
            button.style.backgroundColor = '#7e63ff';
            button.style.backdropFilter = 'blur(15px)'; // Blur efekt
            button.style.border = 'none'; // Sınır olmaması için
            button.style.padding = '10px 15px'; // İçerik etrafındaki boşluk
        // ... ve diğer stiller
            subMenu.appendChild(button);




// Add an event listener to each button

button.addEventListener('click', () => {
    // Arama sorgusunu al
    const query = window.location.search.substr(3);

    // Replace the %s in the URL with the search query

    const url = engine.url.replace('%s', query);

    // Sorguyu arama motoruna yönlendir

    window.location.href = url + query;
});



    // Arama motoru buton renkleri (mouseover & mouseout)

button.addEventListener('mouseover', () => {

    // Mouse üzerine gelindiğinde yapılacak işlemler

    button.style.backgroundColor = '#98FB98'; //
    button.style.color = '';
    button.style.backdropFilter = 'blur(5px)'; // Blur efekt
});

button.addEventListener('mouseout', () => {

    // Mouse düğmenin üzerinden ayrıldığında yapılacak işlemler

    button.style.backgroundColor = '#7e63ff';
    button.style.color = '';
    button.style.backdropFilter = 'blur(5px)'; // Blur efekt
});




                // Arama motorlarına ikon ekle

                if (engine.icon) {
                    const icon = document.createElement('img');
                    icon.src = engine.icon;
                    icon.alt = `${engine.name} Icon`;
                    icon.style.marginRight = '5px'; // Arama motoru ikonu ile metin arasındaki boşluk
                    icon.style.width = '15px'; // Arama motoru ikon boyutu
                    button.insertBefore(icon, button.firstChild);
                }
            }
        });



// Fare üzerine gelince:


// Başlangıçta REX menüsünü gizle

mainMenu.style.display = 'none';



// Arama motorlarını göster & gizle

    // Arama motorlarını göster

    subCategoryButton.addEventListener('mouseover', () => {
        subMenu.style.display = 'block';
    });


    // Arama motorlarını gizle

    subDropdown.addEventListener('mouseleave', () => {
        subMenu.style.display = 'none';
    });


    // Arama motorlarını açık kalsın

    subMenu.addEventListener('mouseenter', () => {
        subMenu.style.display = 'block';
    });
    });


// Kategorileri göster & gizle

    // Kategorileri göster

    mainCategoryButton.addEventListener('mouseover', () => {
    mainMenu.style.display = 'flex';

    // Fareyle üzerine gelindiğinde simgeyi değiştir

    mainCategoryIcon.src = mainCategoryIconUrlHover;
    });


    // Kategorileri gizle

    mainDropdown.addEventListener('mouseleave', () => {
    mainMenu.style.display = 'none';

    // Fareyi dışarı çıkardığınızda simgeyi değiştirin

    mainCategoryIcon.src = mainCategoryIconUrlNormal;
    });


    // Kategorileri açık tut

    mainMenu.addEventListener('mouseenter', () => {
        mainMenu.style.display = 'block';
    });


        // submenu'yu göster

    mainMenu.addEventListener('mouseover', () => {
        mainMenu.style.display = 'flex';
    });
})();