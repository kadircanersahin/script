// ==UserScript==
// @name         Ziyaret Edilen Bağlantıları Vurgula
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Ziyaret edilen bağlantıların rengini değiştirir
// @author       Siz
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = 'a:visited { color: purple !important; }'; // İstediğiniz rengi buraya koyabilirsiniz
    document.head.appendChild(style);
})();