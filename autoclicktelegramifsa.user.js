// ==UserScript==
// @name         KCE: Auto Click (tix)
// @namespace    https://telegram-ifsa.xyz*
// @version      1.0
// @description  Automatically click on a specific element
// @match        *https://www.telegram-ifsa.xyz/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Fuckin element
    var selector = '.separator > a:nth-child(1) > img:nth-child(1)';

    var element = document.querySelector(selector);

    if (element) {
        element.click(1); // Click it
    } else {
        console.log('Element bulunamadı: ' + selector);
    }
})();