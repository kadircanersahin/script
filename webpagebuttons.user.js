// ==UserScript==
// @name         KCE: Webpage Buttons
// @namespace    http://tampermonkey.net/
// @version      1.7
// @description  Add custom buttons and toggle images on a webpage, right-aligned
// @author       Your Name
// @match        http://*/*
// @match        https://*/*
// @grant        GM_openInTab
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

(function() {
    'use strict';

    // Create the buttons and images
    var topButton = createButton('TOP', function() {
        window.scrollTo(0, 0);
    });

    var copyUrlButton = createButton('URL', function() {
        var url = window.location.href;
        navigator.clipboard.writeText(url).then(function() {
            alert('URL copied to clipboard: ' + url);
        }).catch(function(err) {
            console.error('Unable to copy URL to clipboard', err);
        });
    });

    var closeTabButton = createButton('CLS', function() {
        window.open('', '_self', '');
        window.close();
    });

    var bottomButton = createButton('BTM', function() {
        window.scrollTo(0, document.body.scrollHeight);
    });

    var toggleImagesButton = createButton('IMG', function() {
        var images = document.querySelectorAll('img');
        images.forEach(function(image) {
            image.style.display = (image.style.display === 'none') ? '' : 'none';
        });
    });

    // Create a container div for the buttons
    var container = document.createElement('div');
    container.style.position = 'fixed';
    container.style.top = '50%';
    container.style.right = '0';
    container.style.transform = 'translateY(-50%)';
    container.style.zIndex = '9999';

    // Append buttons to the container
    container.appendChild(topButton);
    container.appendChild(copyUrlButton);
    container.appendChild(closeTabButton);
    container.appendChild(bottomButton);
    container.appendChild(toggleImagesButton);

    // Append the container to the body
    document.body.appendChild(container);

    // Function to create a button
    function createButton(label, onClick) {
        var button = document.createElement('button');
        button.textContent = label;
        button.style.display = 'block';
        button.style.marginBottom = '5px';
        button.style.textAlign = 'right'; // Align the text to the right
        button.style.width = '100%'; // Make the button full width
        button.addEventListener('click', onClick);
        return button;
    }
})();
