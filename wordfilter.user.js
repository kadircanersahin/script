// ==UserScript==
// @name         KCE: Word Filter
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Replace or block any words on a webpage
// @author       You
// @match        *://*/*
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';

    // URL of the word list file on GitHub
    var wordListURL = 'https://codeberg.org/kadircanersahin/webfilter/raw/branch/main/wordlisttr.txt';
    var wordRegex;

    // Function to replace bad words in a given text
    function filterText(text) {
        return text.replace(wordRegex, "ilgilenmiyorum");
    }

    // Function to filter all text nodes on the page
    function filterPage() {
        var allTextNodes = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null, false);
        var currentNode;

        while (currentNode = allTextNodes.nextNode()) {
            currentNode.nodeValue = filterText(currentNode.nodeValue);
        }
    }

    // Fetch the word list from GitHub
    GM_xmlhttpRequest({
        method: 'GET',
        url: wordListURL,
        onload: function(response) {
            if (response.status === 200) {
                // Parse the response text as a list of words
                var remoteWordList = response.responseText.split('\n').map(function(word) {
                    return word.trim();
                });

                // Create a regex to match any word in the list
                var wordRegexString = '\\b(' + remoteWordList.join('|') + ')\\b';
                wordRegex = new RegExp(wordRegexString, 'gi');

                // Run the filter on page load
                filterPage();

                // Add an event listener to filter dynamically added content
                document.body.addEventListener('DOMNodeInserted', filterPage);
            }
        }
    });
})();
