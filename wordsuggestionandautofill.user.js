

    // ==UserScript==
    // @name         Word Suggestions and Autofill
    // @namespace    https://tesla.com/
    // @version      1.0
    // @description  Word Suggestions and Autofill.
    // @author       Who needs
    // @match        *://*/*
    // @grant        GM_xmlhttpRequest
    // ==/UserScript==
     
    (function() {
        'use strict';
     
        // Fetch the word list from the provided URL
        GM_xmlhttpRequest({
            method: "GET",
            url: "https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-no-swears.txt",
            onload: function(response) {
                var wordList = response.responseText.split("\n");
     
                // Event listener for the input event on the document
                document.addEventListener("click", function(event) {
                    var target = event.target;
                    var isTextInput = target.tagName === "INPUT" || target.tagName === "TEXTAREA";
     
                    // Hide suggestions box when clicked outside the text input area
                    if (!isTextInput) {
                        document.querySelectorAll('.suggestions-box').forEach(function(box) {
                            box.style.display = "none";
                        });
                        return;
                    }
     
                    // Create a box to display word suggestions
                    var suggestionsBox = document.createElement("div");
                    suggestionsBox.classList.add("suggestions-box");
                    suggestionsBox.style.border = "1px solid #ccc";
                    suggestionsBox.style.padding = "5px";
                    suggestionsBox.style.position = "absolute";
                    suggestionsBox.style.zIndex = "9999"; // Ensure the suggestions box appears in front of other elements
                    suggestionsBox.style.background = "rgba(153, 153, 153, 0.1)"; // Set suggestion box color with 10% opacity
                    suggestionsBox.style.display = "none";
                    suggestionsBox.style.borderRadius = "7px";
                    target.parentNode.insertBefore(suggestionsBox, target.nextSibling);
     
                    // Store the current word being typed
                    var currentWord = '';
     
                    // Event listener for the input event on the text input area
                    target.addEventListener("input", function(event) {
                        var inputValue = target.value.toLowerCase();
                        var caretPos = target.selectionStart; // Get the cursor position
                        var lastSpaceIndex = inputValue.lastIndexOf(' ', caretPos - 1);
     
                        if (lastSpaceIndex === -1) {
                            currentWord = inputValue.slice(0, caretPos);
                        } else {
                            currentWord = inputValue.slice(lastSpaceIndex + 1, caretPos);
                        }
     
                        var suggestions = [];
     
                        // Filter the word list based on the input value
                        wordList.forEach(function(word) {
                            if (word.toLowerCase().startsWith(currentWord)) {
                                suggestions.push(word);
                            }
                        });
     
                        // Limit the suggestions to 5
                        suggestions = suggestions.slice(0, 5);
     
                        // Display the suggestions in the box
                        suggestionsBox.innerHTML = "";
                        suggestions.forEach(function(suggestion) {
                            var suggestionItem = document.createElement("div");
                            suggestionItem.textContent = suggestion;
                            suggestionItem.style.width = suggestion.length + "ch"; // Set suggestion box width to match the length of the word
                            suggestionsBox.appendChild(suggestionItem);
                        });
     
                        // Show/hide the suggestions box based on the input value
                        if (suggestions.length > 0) {
                            suggestionsBox.style.display = "block";
                            // Position the suggestion box at the location of the blinking cursor
                            var rect = target.getBoundingClientRect();
                            suggestionsBox.style.top = (window.scrollY + rect.top + target.offsetHeight) + "px";
                            suggestionsBox.style.left = (window.scrollX + rect.left) + "px";
                        } else {
                            suggestionsBox.style.display = "none";
                        }
                    });
     
                    // Event listener to handle selecting a suggestion
                    suggestionsBox.addEventListener("click", function(event) {
                        var selectedSuggestion = event.target.textContent;
                        var inputValue = target.value.toLowerCase();
                        var caretPos = inputValue.length; // Get the cursor position to the end of the text
     
                        var prefix = inputValue + selectedSuggestion + ' ';
     
                        target.value = prefix;
                        suggestionsBox.style.display = "none";
                        target.focus();
     
                        // Move the cursor to the end of the text
                        target.setSelectionRange(caretPos, caretPos);
                    });
     
                    // Event listener for the Tab key to add the first suggestion automatically
                    target.addEventListener("keydown", function(event) {
                        if (event.key === "Tab") {
                            event.preventDefault();
                            var firstSuggestion = suggestionsBox.querySelector("div");
                            if (firstSuggestion) {
                                var inputValue = target.value.toLowerCase();
                                var caretPos = target.selectionStart; // Get the cursor position
                                var lastSpaceIndex = inputValue.lastIndexOf(' ', caretPos - 1);
     
                                var prefix = '';
                                if (lastSpaceIndex !== -1) {
                                    prefix = inputValue.slice(0, lastSpaceIndex + 1);
                                }
     
                                var suffix = inputValue.slice(caretPos); // Get the rest of the string after the cursor position
     
                                target.value = prefix + firstSuggestion.textContent + ' ' + suffix;
                                suggestionsBox.style.display = "none";
                                target.focus();
                            }
                        }
                    });
                });
            }
        });
    })();

