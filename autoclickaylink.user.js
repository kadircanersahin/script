// ==UserScript==
// @name         KCE: Autoclick (Aylink)
// @namespace    *https://www.aylink.co/*
// @version      1.0
// @description  Automatically click on a specific element
// @match        *https://www.aylink.co/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Motherfuckin element
    var selector = '.btn';

    var element = document.querySelector(selector);

    if (element) {
        element.click(1); // Click it
    } else {
        console.log('Element bulunamadı: ' + selector);
    }
})();